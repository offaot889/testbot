import requests
import json
import pprint
response_API = requests.get('http://air4thai.pcd.go.th/services/getNewAQI_JSON.php?')
data = response_API.text 
a = json.loads(data) 
#print(a)


#บันทึกลง json
""" q = []
for x in a['stations']: 
    w = {}
    w['stationID'] = x['stationID']
    w['lat'] = x['lat']
    w['long'] = x['long']
    q.append(w)
with open("dataapi.json", "r+", encoding="utf-8") as file:  
    file.seek(0)
    json.dump(q, file, ensure_ascii=False, indent=3)

print(q) """      


#a = [{'stationID': '02t', 'lat': '13.732846', 'long': '100.487662'}] 

lat = '{}'.format(16.820833)
long = '{}'.format(100.258056)
f = open('dataapi.json')
data = json.load(f)
#ทำเพื่อค่า stationID
def in_dictlist(lat, value_lat,long,value_long ,my_dictlist):
    for entry in my_dictlist:
        if (entry[lat] == value_lat) and (entry[long] == value_long):
            return entry['stationID']
        else:
            lat_near_new = 0
            long_near_new = 0
            stations_ID = 0
            for s in my_dictlist:
                if (lat_near_new == 0) and (long_near_new == 0) and (stations_ID == 0):
                    lat_near_new = s['lat']
                    long_near_new = s['long']
                    station_ID = 0
                elif (lat_near_new != 0) and (long_near_new != 0):
                    v_lat = abs(float(s['lat']) - float(value_lat))
                    v_long = abs(float(s['long']) - float(value_long))  
                    v_lat_new = abs(float(lat_near_new) - float(value_lat))  
                    v_long_new = abs(float(long_near_new) - float(value_long))
                    if (v_lat < v_lat_new) and (v_long < v_long_new):
                        lat_near_new = s['lat']
                        long_near_new = s['long']
                        station_ID = s['stationID']
    return station_ID        
s_id = (in_dictlist('lat',lat,'long',long, data))
print(s_id)
response_API = requests.get('http://air4thai.pcd.go.th/services/getNewAQI_JSON.php?stationID={}'.format(s_id))
data = response_API.text 
data_api = json.loads(data)
print(data_api)
for d_api in data_api:
    Pm25 = data_api['LastUpdate']['PM25']['value'] + data_api['LastUpdate']['PM25']['unit']
    Date = data_api['LastUpdate']['date'] 
    Time = data_api['LastUpdate']['time']
    Aqi =  data_api['LastUpdate']['AQI']['aqi']

print(Pm25, Date, Time, Aqi)  


""" q = []
for x in a['stations']: 
    q.append(x['stationID'])
print(q)
print(len(q))  """

""" import datetime

original_date = datetime.datetime.strptime("2021-01-11", '%Y-%m-%d')
formatted_date = original_date.strftime("%d:%m:%Y")
print(formatted_date) """

  
#with open('bubble_greeting.json', 'r', encoding="utf-8") as bbg:
#   data_bbg = json.load(bbg)
 #   x = '"""{}"""'.format(data_bbg)
#print(x)

#with open ('bubble_greeting.txt', 'rt', encoding="utf-8") as myfile:  
 #       data_bbg = myfile.read()
 #       #bubble_string = '"""{}"""'.format(data_bbg) 
 #       bubble_string = '"""{}"""'.format((f"""{data_bbg}"""))
 #       print(bubble_string)  
                            
#with open("r.json", 'r' ,encoding="utf-8") as file:
    #data = json.load(file)
    #x = '"""{}"""'.format(data)
    #y=pprint.pprint(data).replace("'", '"')
    #print(data)
    
    

#str = x.replace("\'", "\"").replace("True", "true")
#str1 = str
#print(str)





bubble_string = """{
    "type": "carousel",
    "contents": [
        {
            "type": "bubble",
            "hero": {
                "type": "image",
                "url": "https://cdn1.vectorstock.com/i/1000x1000/01/45/flood-rescue-vector-19930145.jpg",
                "size": "full",
                "aspectRatio": "20:13",
                "aspectMode": "cover"
            },
            "body": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#AFE6F1FF",
                "contents": [
                    {
                        "type": "text",
                        "text": "ปัญหาน้ำท่วม",
                        "weight": "bold",
                        "size": "xl",
                        "align": "center",
                        "wrap": true,
                        "contents": []
                    },
                    {
                        "type": "text",
                        "text": "Flood problem",
                        "weight": "bold",
                        "align": "center",
                        "contents": []
                    }
                ]
            },
            "footer": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#AFE6F1FF",
                "contents": [
                    {
                        "type": "button",
                        "action": {
                            "type": "message",
                            "label": "แจ้งปัญหาน้ำท่วม",
                            "text": "แจ้งปัญหาน้ำท่วม"
                        },
                        "style": "primary"
                    }
                ]
            }
        },
        {
            "type": "bubble",
            "hero": {
                "type": "image",
                "url": "https://media.istockphoto.com/vectors/road-surface-repair-vector-id908629216?k=20&m=908629216&s=612x612&w=0&h=I_9xoYdlwLsm4OQ5vgMee47GO5tJ8o4ewuBncrCHPwI=",
                "size": "full",
                "aspectRatio": "20:13",
                "aspectMode": "cover"
            },
            "body": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#FAFABBFF",
                "contents": [
                    {
                        "type": "text",
                        "text": "ปัญหาเกี่ยวกับถนน",
                        "weight": "bold",
                        "size": "xl",
                        "align": "center",
                        "wrap": true,
                        "contents": []
                    },
                    {
                        "type": "text",
                        "text": "Road problem",
                        "weight": "bold",
                        "align": "center",
                        "contents": []
                    }
                ]
            },
            "footer": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#FAFABBFF",
                "contents": [
                    {
                        "type": "button",
                        "action": {
                            "type": "message",
                            "label": "แจ้งปัญหาถนน",
                            "text": "แจ้งปัญหาถนน"
                        },
                        "flex": 2,
                        "style": "primary"
                    }
                ]
            }
        },
        {
            "type": "bubble",
            "hero": {
                "type": "image",
                "url": "https://leverageedu.com/blog/wp-content/uploads/2019/09/Electrical.png",
                "size": "full",
                "aspectRatio": "20:13",
                "aspectMode": "cover"
            },
            "body": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#E5F6C0FF",
                "contents": [
                    {
                        "type": "text",
                        "text": "ปัญหาไฟฟ้า",
                        "weight": "bold",
                        "size": "xl",
                        "align": "center",
                        "wrap": true,
                        "contents": []
                    },
                    {
                        "type": "text",
                        "text": "Electrical problem",
                        "weight": "bold",
                        "align": "center",
                        "contents": []
                    }
                ]
            },
            "footer": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#E5F6C0FF",
                "contents": [
                    {
                        "type": "button",
                        "action": {
                            "type": "message",
                            "label": "แจ้งปัญหาไฟฟ้า",
                            "text": "แจ้งปัญหาไฟฟ้า"
                        },
                        "style": "primary"
                    }
                ]
            }
        },
        {
            "type": "bubble",
            "hero": {
                "type": "image",
                "url": "https://c.neh.tw/thumb/f/720/comvecteezy297944.jpg",
                "size": "full",
                "aspectRatio": "20:13",
                "aspectMode": "cover"
            },
            "body": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#DFC9AFFF",
                "contents": [
                    {
                        "type": "text",
                        "text": "ปัญหาขยะ",
                        "weight": "bold",
                        "size": "xl",
                        "align": "center",
                        "wrap": true,
                        "contents": []
                    },
                    {
                        "type": "box",
                        "layout": "baseline",
                        "flex": 1,
                        "contents": [
                            {
                                "type": "text",
                                "text": "Garbage problem",
                                "weight": "bold",
                                "size": "md",
                                "align": "center",
                                "contents": []
                            }
                        ]
                    }
                ]
            },
            "footer": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "backgroundColor": "#DFC9AFFF",
                "contents": [
                    {
                        "type": "button",
                        "action": {
                            "type": "message",
                            "label": "แจ้งปัญหาขยะ",
                            "text": "แจ้งปัญหาขยะ"
                        },
                        "flex": 2,
                        "style": "primary"
                    }
                ]
            }
        }
    ]
}"""

""" with open('r.json', 'w', encoding='utf-8') as f:
    json.dump( bubble_string, f, ensure_ascii=False, indent=None)  """

#with open('r.json',encoding="utf8") as json_file:
 #   data = json.load(json_file)
 #   bbs = '"""{}"""'.format(data) 
 #   print(bbs) 

""" with open('bubble_greeting.txt', encoding='utf-8') as f:
    data = f.readlines()
print(data )"""


